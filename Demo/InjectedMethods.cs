﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Speech.Synthesis;
using System.Text;

namespace Demo
{
	public class InjectedMethods
	{
		private static SpeechSynthesizer _speaker = null;
		private static SpeechSynthesizer Speaker
		{
			get
			{
				if (_speaker == null)
				{
					_speaker = new SpeechSynthesizer();
					_speaker.Volume = 100;// 音量 0到100
					_speaker.Rate = 2; // 速度 -10 到 10
					//_speaker.SelectVoice("Microsoft Huihui Desktop");
				}
				return _speaker;
			}
		}

		private static Prompt prompt = null;


		public void Speak(string msg)
		{
			if (prompt != null && prompt.IsCompleted == false)
			{
				Speaker.SpeakAsyncCancel(prompt);
			}

			prompt = Speaker.SpeakAsync(msg);
		}
	}
}
